﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Only one script implementing this class should be attached to an object at a time
abstract public class Movement : MonoBehaviour
{
    // Assuming horizontal movement only here, takes in axis which should be from 0 to 1
    abstract public void moveSide(float axis, float maxSpeed);
    abstract public void jump(float force);
    abstract public void act();
    abstract public void onPossess();
}
