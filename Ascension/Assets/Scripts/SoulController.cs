﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// list of components that the object with this script must share
[RequireComponent(typeof(Rigidbody2D))]
public class SoulController : MonoBehaviour
{
    [SerializeField, Tooltip("The camera used for viewing the scene, which the character can't leave")]
    private Camera mainCam;
    [SerializeField, Tooltip("The max speed that any movement is limited to")]
    private float speed = 0.2f;
    [SerializeField, Tooltip("The furthest distance away the spirit can possess a downed enemy from")]
    private float possessDist = 1.25f;
    private Vector2 currentVelocity = new Vector2();

    // The latest retrieved raw inputs from the user
    private float horizontal; // Horizontal axis: left stick horizontal axis or a/d or left/right
    private float vertical; // Vertical axis: left stick vertical axis or s/w or down/up
    private bool action; // Context sensitive action: button 1 or space
    private bool eject; // Eject from possession: button 3 or q
    private bool jump; // Jump: w or button 0
    [SerializeField, Tooltip("The object (enemy) that the soul is possessing")]
    public GameObject possessed = null;
    [SerializeField, Tooltip("The abstract class that moves anything possessable")]
    private Movement move = null; // The generic to move whatever gets possessed
    [SerializeField, Tooltip("The animator that drives any possessable object")]
    Animator animator = null; // The animator of whatever object gets possessed
    [SerializeField, Tooltip("The rigidbody of the possessed object")]
    Rigidbody2D possessedRigid = null;

    private Rigidbody2D thisRigidbody2D;

    public Rect cameraBox; // The bounds of the camera that the soul must stay within
    public Vector3 bottomLeft; // Bottom Left corner of cameraBox
    public Vector3 topRight; // Top Right corner of cameraBox

    // Start is called before the first frame update
    void Start()
    {
        thisRigidbody2D = GetComponent<Rigidbody2D>();
        // Establish the bounds of the camera
        bottomLeft = mainCam.ScreenToWorldPoint(Vector3.zero);
        topRight = mainCam.ScreenToWorldPoint(new Vector3(mainCam.pixelWidth, mainCam.pixelHeight));
        cameraBox = new Rect(bottomLeft.x, bottomLeft.y, topRight.x - bottomLeft.x, topRight.y - bottomLeft.y);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // All input axis names can be found in "Edit > Project Settings > Input Manager"
        horizontal = Input.GetAxisRaw("Horizontal");
        // Raw axis has no smoothing transitioning between values; it snaps from 0 to 1 instantly
        // Recommended for button inputs or having individual/custom smoothing
        vertical = Input.GetAxisRaw("Vertical");

        // Buttons should always be 0 or 1, which should map directly to a bool with raw
        action = Input.GetButton("Action");
        eject = Input.GetButton("Eject");
        jump = Input.GetButton("Jump");    

        // gameObject and transform refer to whatever this script is attached to
        // Adding on to a position for movement will ignore collisions and act strangely with any applied force/velocity
        //gameObject.transform.position = new Vector2(transform.position.x + (horizontal * speed), transform.position.y + (vertical * speed));

        // If the soul is not currently controlling anything
        if (!possessed)
        {
            // Prevents the soul from leaving the camera
            // Could have an offset added on top of this as well
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, cameraBox.xMin, cameraBox.xMax),
                                             Mathf.Clamp(transform.position.y, cameraBox.yMin, cameraBox.yMax));
            // Handle movement by pushing the soul using the user's input
            thisRigidbody2D.AddForce(new Vector2(horizontal*speed, vertical*speed));

            if (action)
            {
                // Finds if an enemy is downed and in range to be possessed
                possessed = closestTagWithin("Downed", possessDist);

                // Checks if something was actually possessed, since null can be returned
                if (possessed)
                {
                    // Makes the soul locked to the possessed object, since we'll be moving the possessed object instead now
                    transform.position = possessed.transform.position;
                    transform.parent = possessed.transform;
                    possessed.tag = "Player";
                    // Also make the soul (sprite) disappear
                    GetComponent<SpriteRenderer>().enabled = false;
                    thisRigidbody2D.simulated = false;

                    // Retrieve the animator of the possessed object
                    animator = possessed.GetComponentInChildren<Animator>();
                    // Sets the object to be possessed in the animator so that it gets up
                    animator.SetBool("possession", true);

                    possessedRigid = possessed.GetComponent<Rigidbody2D>();
                    // Establishes how to move the possessed object
                    move = possessed.GetComponent<Movement>();
                }
            }
        }
        else
        {
            // Prevents the character from leaving the camera
            // Could have an offset added on top of this as well
            possessed.transform.position = new Vector3(Mathf.Clamp(possessed.transform.position.x, cameraBox.xMin, cameraBox.xMax),
                                             Mathf.Clamp(possessed.transform.position.y, cameraBox.yMin, cameraBox.yMax));
            // Gets how fast the possessed object is moving
            currentVelocity = possessedRigid.velocity;
            // End possession when user inputs eject button
            // Nested if statements achieve prioritized inputs
            // And prevents the effects of some inputs (moving) from overlapping with others
            if (eject)
            {
                // Turns off other animator flags so that the object will actually reach the downed state
                // TRIGGERS MIGHT FIX THIS WITHOUT REQUIRING THIS SET
                animator.SetBool("punching", false);
                animator.SetBool("running", false);
                // Returns the object to downed state
                animator.SetBool("possession", false);
                possessed.tag = "Downed";
                //possessed.SetActive(false); // Removes corpse completely
                // Cleans up the references to the possessed object
                possessed = null;
                transform.parent = null;
                animator = null;
                move = null;

                // Displays soul again and allows it to move on its own again
                GetComponent<SpriteRenderer>().enabled = true;
                thisRigidbody2D.simulated = true;
            }
            else
            {
                if (jump)
                {
                        
                }
                else
                {
                    // This has problems of calling act every frame while the button is held
                    // Once anything past the animation is implemented, this will need to change
                    if (action)
                    {
                        animator.SetBool("punching", true);
                        move.act();
                    }
                    // Consider using triggers instead of booleans to alleviate many issues
                    else
                    {
                        animator.SetBool("punching", false);

                        // Need deadzone so that running animation isn't always playing
                        if (horizontal < -0.1 || horizontal > 0.1)
                        {
                            animator.SetBool("running", true);
                            move.moveSide(horizontal, speed);
                        }
                        else
                            animator.SetBool("running", false);
                    }
                }
            }
        }
    }
    
    // Finds the closest object with the given tag to the object running this script
    //Input:
    // tag - string of the tag to be searched for
    // max - maximum distance to accept an object within (float)
    //Output:
    // closest - GameObject with tag that is closest to the object running this script and within the maximum acceptable distance
    // OR null - this function can return null when there are no objects with the tag, or none of the tagged objects are within the maximum acceptable distance
    private GameObject closestTagWithin(string tag, float max)
    {
        // Retrieves every Game Object with the given tag
        GameObject[] objectList = GameObject.FindGameObjectsWithTag(tag);
        GameObject closest = null;

        float closestDistance = float.MaxValue; // Distance to closest object, important to not recalculate constantly and to check against minimum radius
        float tempDistance; // Storage for comparing other objects' distances against the closest one's

        // Iterate over every Game Object with the given tag
        foreach (GameObject obj in objectList)
        {
            // Ensures that closest is populated with something (the first object in the list)
            if (!closest)
            {
                closest = obj;
                // Gets distance from this object to the first object in the list
                closestDistance = Vector3.Distance(this.transform.position, obj.transform.position);
            }
            else
            {
                // Gets the distance from this object to the object being checked
                tempDistance = Vector3.Distance(this.transform.position, obj.transform.position);
                // Checks if the new object is closer than the current best
                if (tempDistance < closestDistance)
                {
                    // Replaces the old best if closer
                    closest = obj;
                    closestDistance = tempDistance;
                }
            }
        }
        // Make sure closest is within the minimum distance away to find an object within
        if (closestDistance > max)
            closest = null;

        return closest;
    }
}
