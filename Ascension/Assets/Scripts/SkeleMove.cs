﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeleMove : Movement
{
    private Rigidbody2D thisRigidBody2D;
    private bool facingRight;
    Animator animator;
    private void Start()
    {
        thisRigidBody2D = GetComponent<Rigidbody2D>();
        // Determine initial facing direction with buffer to allow for floating point error
        if (transform.localRotation.eulerAngles.y >= 179 && transform.localRotation.eulerAngles.y <= 181)
            facingRight = false;
        else
            facingRight = true;
        // Because this script will be on the parent object (prefab), the animator must be retrieved from the child rig
        animator = GetComponentInChildren<Animator>();
    }

    public override void onPossess()
    {
        animator.SetBool("possession", true);
    }

    public override void act()
    {
        // To-implement: creating a punching hitbox at some point during the animation
        // Requires a full health system to be implemented too
    }

    public override void jump(float force)
    {
        
    }

    // Uses the value from the axis as a multiplier for the max speed
    // Applies a force to the skeleton to make it move
    public override void moveSide(float axis, float maxSpeed)
    {
        // axis less than 0 should be an input of d, left, or left on the stick
        if (axis < 0)
        {
            facingRight = false;
            transform.localRotation = Quaternion.Euler(0, 180, 0);
        }
        // Otherwise we know the opposite is true
        else
        {
            facingRight = true;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        thisRigidBody2D.AddForce(new Vector2(axis*maxSpeed,0));
    }
}
